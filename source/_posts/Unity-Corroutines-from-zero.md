---
title: Unity Corroutines from zero
date: 2023-06-12 17:54:03
tags:
---
# Intro

In this study we will try to create something similar to unity coroutines using some of the same techniques

# IEnumerable are made to be used in foreach!

Lets say we have a Square class

``` C#
using System.Numerics;

  
class Square
{
    public Vector2 Corner1;
    public Vector2 Corner2;
    public Vector2 Corner3;
    public Vector2 Corner4;

  
    public Square(Vector2 corner1, Vector2 corner2,Vector2 corner3, Vector2 corner4)
    {
        Corner1 = corner1;
        Corner2 = corner2;
        Corner3 = corner3;
        Corner4 = corner4;
    }
}
```

Now let's say we want to print all the X coordinates for our square we can certainly do something like this

``` C#
var square = new Square(new Vector2(0,0), new Vector2(1,0), new Vector2(1,2), new Vector2(0,1));

Console.Write(square.Corner1.X + " ");
Console.Write(square.Corner2.X + " ");
Console.Write(square.Corner3.X + " ");
Console.Write(square.Corner4.X + " ");
```

this certainly works, but this way of writing code don't scale very well for other shapes, for example do you know in your head for example how many CornerN we would need to write to iterate over all of the corners of an icosahedron, that would not look so good would it?

But we could create a IEnumerableFunction to help us here, lets redefine the square class with a new function called Corners:

``

``` C#
using System.Numerics;

struct Square
{
    public Vector2 Corner1;
    public Vector2 Corner2;
    public Vector2 Corner3;
    public Vector2 Corner4;

    public Square(Vector2 corner1, Vector2 corner2,Vector2 corner3, Vector2 corner4)
    {
        Corner1 = corner1;
        Corner2 = corner2;
        Corner3 = corner3;
        Corner4 = corner4;
    }

    public IEnumerable<Vector2> Corners()
    {
        yield return Corner1;
        yield return Corner2;
        yield return Corner3;
        yield return Corner4;
    }
}
```

we could now iterate over the corners in a less repetitive way!


``` C#
var square = new Square(new Vector2(0,0), new Vector2(1,0), new Vector2(1,2), new Vector2(0,1));

foreach(Vector2 corner in square.Corners())
{
    Console.Write(corner.X + " ");
}
```

The end user of our class doesn't even have to know how many corners are in a square anymore!

Actually, IEnumerable methods is a great way to simplifly nested loops

And we didn't have to create any memory garbage heavy structure like Lists or arrays to iterate over!

but wait what is happening here? i mean, looking at my signature my function should return a `IEnumerable<int>` object and we are returning only int and i leaned in school that the function stop after the first return, and what are those yields???

well there is some compiler magic going on here, lets explore this!

taking a brief look at a very specific part of the [IEnumerator Documentation](https://learn.microsoft.com/en-us/dotnet/api/system.collections.generic.ienumerable-1?view=net-8.0)

> `IEnumerable<T>` contains a single method that you must implement when implementing this interface; GetEnumerator, which returns an `IEnumerator<T>` object. The returned `IEnumerator<T>` provides the ability to iterate through the collection by exposing a Current property.

So something like this:

``` C#
interface IEnumerable<T>
{
    IEnumerator<T> GetEnumerator();
}
```

for a type to implement `IEnumerator<T>` it contain the following members:

``` C#
interface IEnumerator<T>
{
    T Current {get;}
    bool MoveNext();
    void Reset();
}

```

so we could rewrite our enumerable object like this to make the use of an `IEnumerable<Vector2>` object explicit

``` C#
IEnumerable<Vector2> enumerable = square.Corners();
foreach(Vector2 corner in enumerable)
{
    Console.Write(corner.X + " ");
}
```

this is actually being expanded to something like this, and actually we could write exactly that to the same effect of the iterator

``` C#
IEnumerable<Vector2> enumerable = square.Corners();
IEnumerator<Vector2> enumerator = enumerable.GetEnumerator();

while(enumerator.MoveNext())
{
    Vector2 corner = enumerator.Current;
    Console.Write(corner.X + " ");
}
```

# Going Deeper

things are gonna get a little intense here and if you just want to implement your own version of Unity coroutines you can skip to the next section 

Now, where did these variables came from `enumerable` e `enumerator`? and what are their types?

well, lets find out

``` C#
Console.WriteLine(enumerator.GetType());
```

Well, let's take it easy `Submission#N` is just an artifact of the interactive C# session that we are using, what we would get exploring an actual [compile C# version](https://sharplab.io/#v2:D4AQDABCCMDcCwAocVoBYGKQOwIYFsBTAZwAdcBjQiACUIBt6B7AdSYCd6ATJAbyQiCoAJlQB2AUP6Ihs1ADZUAZgA8AS2wAXAHwQA4oU0A5AK74ARoXbEAFAEpJciNKdOY0KGIhhMrue88IOEc/VA8QL2FfVwBfJBDZGEUQNAgAWVwNGxgwAG0AXQhcdgBzYgcZVxdQwQA3YohsM0trCABefUNTCytbO2iamABOGyae6wA6A00AFQBPUkJ7CaMCQn6EoTjKwW2YoA==) of our code is an internal class that implements a state machine!

so the compiler took our method and created a new class that does all those things we expected our code to do, so if we would create something similar by hand it could be something like this:

there are some extra stuff there so we can--------------------

``` C#
using System.Numerics;

// our base class
class Square
{
    public Vector2 Corner1;
    public Vector2 Corner2;
    public Vector2 Corner3;
    public Vector2 Corner4;

    public Square(Vector2 corner1, Vector2 corner2,Vector2 corner3, Vector2 corner4)
    {
        Corner1 = corner1;
        Corner2 = corner2;
        Corner3 = corner3;
        Corner4 = corner4;
    }

    public IEnumerable<Vector2> Corners()
    {
        return new SquareCorenersEnumerable(this);
    }
}

// an Enumerable class
class SquareCorenersEnumerable : IEnumerable<Vector2>
{
    private Square square;
    public SquareCorenersEnumerable(Square square)
    {
        this.square = square;
    }

    public IEnumerator<Vector2> GetEnumerator()
    {
        return new SquareCorners(square);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

// an Enumerator class
class SquareCorners: IEnumerator<Vector2>
{
    private Square square;
    private int currentIndex = 0;
    
    public SquareCorners(Square square)
    {
        this.square = square;
    }


    public bool MoveNext()
    {
        if (currentIndex >= 4)
        {
            return false;
        }
        currentIndex++;
        return true;

    }

    public void Reset()
    {
        currentIndex = 0;
    }

    public Vector2 Current =>
        currentIndex switch
        {
            1 => square.Corner1,
            2 => square.Corner2,
            3 => square.Corner3,
            _ => square.Corner4
        };

    object IEnumerator.Current => Current;

    public void Dispose()
    {
    }
}

var square = new Square(new Vector2(0,0), new Vector2(1,0), new Vector2(1,2), new Vector2(0,1));


// then we could come back to our "simple" foreach  that we wrote before
foreach(Vector2 corner in square.Corners())
{
    Console.Write(corner.X + " ");
}
```

TL DR: the compile implementation with yields to a state machine so it can track the progress of our method, there is a hidden class that is instantiated, updated and destroyed in the process of our foreach that translates to what appears to be a method that can rave multiple returns and run on part at a time.

# The Trick

well, we now have a powerful tool to create these IEnumerator Objects that we can iterate over with MoveNext and Current, the thing is, nobody said that we are limited to run it sequentially!

lets create an object with a simple update loop, lets imagine that it is called every frame in our application:

``` C#
public class Updatable
{
    public void Update()
    {

    }
}
```

Now let create a method that we want to run piece by piece every frame, something that animates is a good example

``` C#
public class AnimatedSprite 
{
    public int CurrentFrame;

    public IEnumerable Animate()
    {
        while (true)
        {
            CurrentFrame = 0;
            yield return null;
            CurrentFrame = 1;
            yield return null;
            CurrentFrame = 2;
            yield return null;
            CurrentFrame = 3;
            yield return null;
            CurrentFrame = 4;
            yield return null;
            CurrentFrame = 5;
            yield return null;
        }
    }
}
```

Now what we could do is take our updatable class and call one MoveNext every update cycle keeping a reference to our IEnumerator

``` C#
public class AnimationUpdater
{
    IEnumerator animation;

    public AnimationUpdater(AnimatedSprite animatedSprite)
    {
        animation = animatedSprite.Animate().GetEnumerator();
    }

    public void Update()
    {
        animation.MoveNext();
    }
}
```

Actually since e are not using a foreach loop either way we can get rid of our IEnumerable.GetEnumerator and pass directly a Enumerator

``` C#
public class AnimatedSprite 
{
    public int CurrentFrame;

    public IEnumerator Animate()
    {
        while (true)
        {
            CurrentFrame = 0;
            yield return null;
            CurrentFrame = 1;
            yield return null;
            CurrentFrame = 2;
            yield return null;
            CurrentFrame = 3;
            yield return null;
            CurrentFrame = 4;
            yield return null;
            CurrentFrame = 5;
            yield return null;
        }
    }

}

public class AnimationUpdater
{
    IEnumerator animation;

    public AnimationUpdater(AnimatedSprite animatedSprite)
    {
        animation = animatedSprite.Animate();
    }

    public void Update()
    {
        animation.MoveNext();
    }
}
```

that is nice be we will run into problems if we pass something that does eventually return, so lets update our code to have a sprite that only animate once, luckily the ability to know if a IEnumerator ran its course is baked in the return of the MoveNext function

so we could do something like this and we would be golden!

``` C#
public class AnimatedSprite 
{
    public int CurrentFrame;


    public IEnumerable Animate()
    {
            CurrentFrame = 0;
            yield return null;
            CurrentFrame = 1;
            yield return null;
            CurrentFrame = 2;
            yield return null;
            CurrentFrame = 3;
            yield return null;
            CurrentFrame = 4;
            yield return null;
            CurrentFrame = 5;
            yield return null;
    }
}

public class AnimationUpdater
{
    IEnumerator animation;

    public void  PlayAnimation(AnimatedSprite animatedSprite)
    {
        animation = animatedSprite.Animate().GetEnumerator();
    }

    public void Update()
    {
        if(animation != null)
        {
            if(animation.MoveNext())
            {
                // do nothing
            }
            else 
            {
                animation = null;
            }
        } 
    }
}
```

now there is one last thing we are ignoring, our IEnumerators are returning null every time, now lets recap why yield must always return something even if it is null? because it was conceived to be a way to iterate over some kind set.

We are far indeed from the expected use of the feature, but it doesn't mean that we cannot put the return value of the iterator to good use, in frame animation sometimes we ant to pass more or less time in the same frame to give more impact, we could for example use the return of the yield to say how much time to be in each frame!

``` C#
public class AnimatedSprite 
{
    public int CurrentFrame;

    public IEnumerable<int> Animate()
    {
            CurrentFrame = 0;
            yield return 1;
            CurrentFrame = 1;
            yield return 1;
            CurrentFrame = 2;
            yield return 3;
            CurrentFrame = 3;
            yield return 2;
            CurrentFrame = 4;
            yield return 1;
            CurrentFrame = 5;
            yield return 1;
    }
}

public class AnimationUpdater
{
    IEnumerator<int> animation;
    int framesToNextUpdate;

    public void  PlayAnimation(AnimatedSprite animatedSprite)
    {
        animation = animatedSprite.Animate().GetEnumerator();
    }

    public void Update()
    {
        if(animation != null)
        {
            framesToNextUpdate --;
            if(framesToNextUpdate <= 0)
            {
                if(animation.MoveNext())
                {
                    framesToNextUpdate = animation.Current;
                }
                else 
                {
                    animation = null;
                }
            }
            
        } 
    }
}
```

From here we can expand our implementation so eventually we can do everything that unity does with coroutines, we can expand the idea of the returned object to make so it always contains information about what we want to wait until the next MoveNext, this way we can for example  create a condition that must be true before we continue, this way we can do WaitForSeconds, WaitEndOfFrame, WaitLifeSmallerThen0(we could for example check every frame the and move next depending on the player life)

  

to stop a coroutine we could clear the enumerator reference and the rest would never be called

# Conclusion

There you have it, you can now make your own version of Unity Coroutines, now should you?

No!

How to make it is not the point of this article, the important takeaway is that it is somewhat of a hacky implementation, coroutines, were not the intended use of IEnumerable


Coroutine were created in the past, they are of course better than nothing but now we have more robust ways to implement asynchronous code in C# (mainly `Awaitables`) that end up in a much cleaner implementation that we can use if we are creating our own engine.

And if you are using unity, well, since async is still a little hit and miss you can use some library like [UniTask](https://github.com/Cysharp/UniTask) that integrate Awaitables painlessly (or almost) in unity


Now what i want you to take away is to IEnumerables for what they were conceived Iterators and that is where they really shine! and i think are a feature that is grossly underused. 

there is an unique pleasure in extracting the loops of a heavily nested iteration and and ending up with some code like this

``` C#
foreach (Character character in WorldMap.Characters())
{
    ApplySomeEffect(character);
}

```

instead of

```C#
foreach (Area area in WorldMap.Areas)
{
    for (int x = 0; x < area.Cells.Count; x++)
    {
        for (int y = 0; y < area.Cells[x].Count; x++)
        {
            var cell = area.Cells[x][y];
            if (cell.Characters != null )
            {
                foreach (Character character in cell.Characters)
                {
                    ApplySomeEffect(character);
                }
            }
        }
    }
}
```